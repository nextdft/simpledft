# logo

Vector and raster images of the SimpleDFT logo.

The logo uses a modified [Comfortaa](https://fonts.google.com/specimen/Comfortaa) font.

The following color palette has been used

| Colors    |
| :-------: |
| `#7e0019` |
| `#b5123e` |
| `#ed5168` |
